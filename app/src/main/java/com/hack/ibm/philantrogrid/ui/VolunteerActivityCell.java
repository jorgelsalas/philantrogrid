package com.hack.ibm.philantrogrid.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hack.ibm.philantrogrid.R;
import com.hack.ibm.philantrogrid.constants.GeneralConstants;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;

/**
 * Created by Jorge on 28/09/2015.
 */
public class VolunteerActivityCell extends RelativeLayout {

    private VolunteerActivity activity;
    private TextView name;
    private TextView description;

    public Button getParticipateButton() {
        return participateButton;
    }

    private Button participateButton;
    private ImageView image;

    public VolunteerActivityCell(Context context) {
        super(context);
    }

    public VolunteerActivityCell(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public VolunteerActivityCell(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate(){
        super.onFinishInflate();
        this.name = (TextView) this.findViewById(R.id.activity_name);
        this.description = (TextView) this.findViewById(R.id.activity_description);
        this.image = (ImageView) this.findViewById(R.id.activity_image);
        this.participateButton = (Button) this.findViewById(R.id.participate_button);
    }

    public void setModel(VolunteerActivity volunteerActivity){
        this.activity = volunteerActivity;
        updateUI();
    }

    private void updateUI(){
        name.setText(activity.getName());
        description.setText(activity.getDescription());
        Bitmap placeholderImage;
        switch (activity.getType()){
            case(GeneralConstants.ACTIVITY_TYPE_EDUCATIONAL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.education);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_ENVIRONMENTAL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.enviroment);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_GOOD_WILL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.icon);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_HEALTH):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.health);
                break;

            default:
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.placeholder);
                break;

        }
        image.setImageBitmap(placeholderImage);
        //TODO: Change bitmap depending on type of activity
    }

}
