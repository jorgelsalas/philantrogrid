package com.hack.ibm.philantrogrid.interfaces;

import com.hack.ibm.philantrogrid.entities.VolunteerActivity;

/**
 * Created by Jorge on 29/09/2015.
 */
public interface ActivityDetailsRequestListener {
    void onActivityDetailsRequested(VolunteerActivity activity, boolean hideParticipateButton);
    void onActivityDetailsDismissed();
    void onActivityDetailsDismissed(boolean goToSearch);
}
