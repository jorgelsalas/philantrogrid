package com.hack.ibm.philantrogrid.entities;

import java.util.ArrayList;



/*
 * define O-R mapping of todolist table
 */
public class User {

	long id;
	String name;
	String mail;
    String pass;
    String phone;
    String country;
    String type;
    String gender;
    int age;

    private ArrayList<UserRequest> requests = new ArrayList<UserRequest>();

    public User(){

    }

    public User(long id, String name, String mail, String pass, String phone, String country, String type, String gender, int age) {
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.pass = pass;
        this.phone = phone;
        this.country = country;
        this.type = type;
        this.gender = gender;
        this.age = age;
    }


    
    public User(String name, String mail, String pass, String phone, String country, String type, String gender, int age) {
        this.name = name;
        this.mail = mail;
        this.pass = pass;
        this.phone = phone;
        this.country = country;
        this.type = type;
        this.gender = gender;
        this.age = age;
        this.requests = new ArrayList<UserRequest>();
    }
	
	public long getId() {
		return id;
	}

	public void setId(long pk) {
		id = pk;
	}
	
	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    public ArrayList<UserRequest> getRequests() {
		return requests;
	}

	public void setRequests(ArrayList<UserRequest> requests) {
		this.requests = requests;
	}
    
    @Override
    public String toString() {
        return String.format("{\"id\": \"%d\", " +
                "\"name\": \"%s\", " +
                "\"mail\": \"%s\", " +
                "\"pass\": \"%s\", " +
                "\"phone\": \"%s\", " +
                "\"country\": \"%s\", " +
                "\"type\": \"%s\", " +
                "\"gender\": \"%s\", " +
                "\"age\": \"%d\"" +
                "}", id, name, mail, pass, phone, country, type, gender, age);
    }
    
    public void setsAll(String name, String mail, String pass, String phone, String country, String type, String gender, int age){
        setName(name);
        setMail(mail);
        setPass(pass);
        setPhone(phone);
        setCountry(country);
        setType(type);
        setGender(gender);
        setAge(age);
    }

	//@Override
	//public String toString() {
	//	return String.format("{\"id\": \"%d\", \"name\": \"%s\", \"description\": \"%s\"}", id, name, description);
	//}
	
	
}
