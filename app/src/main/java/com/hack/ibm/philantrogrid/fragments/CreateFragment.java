package com.hack.ibm.philantrogrid.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.hack.ibm.philantrogrid.R;
import com.hack.ibm.philantrogrid.constants.GeneralConstants;
import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.interfaces.OnFragmentInteractionListener;
import com.hack.ibm.philantrogrid.services.BluemixClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    @Bind(R.id.name)  EditText name;
    @Bind(R.id.description)  EditText description;
    @Bind(R.id.disclaimer)  EditText disclaimer;

    @Bind(R.id.min_age)  EditText min_age;
    @Bind(R.id.type) Spinner type;
    @Bind(R.id.desired_gender)  EditText desired_gender;

    @Bind(R.id.contact_phone)  EditText contact_phone;
    @Bind(R.id.contact_mail)  EditText contact_mail;
    @Bind(R.id.contact_name)  EditText contact_name;

    @Bind(R.id.start_date)  EditText start_date;
    @Bind(R.id.end_date)  EditText end_date;
    @Bind(R.id.required_persons)  EditText required_persons;

    @Bind(R.id.location)  EditText location;

    @Bind(R.id.presets_button)  Button usePresetsButton;
    @Bind(R.id.create_button)  Button createButton;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateFragment newInstance(String param1, String param2) {
        CreateFragment fragment = new CreateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create, container, false);;
        ButterKnife.bind(this, v);

        List<String> activityTypes = new ArrayList<>();
        activityTypes.add(GeneralConstants.ACTIVITY_TYPE_EDUCATIONAL);
        activityTypes.add(GeneralConstants.ACTIVITY_TYPE_ENVIRONMENTAL);
        activityTypes.add(GeneralConstants.ACTIVITY_TYPE_GOOD_WILL);
        activityTypes.add(GeneralConstants.ACTIVITY_TYPE_HEALTH);

        ArrayAdapter<String> typesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, activityTypes);
        typesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        type.setAdapter(typesAdapter);
        type.getSelectedItem().toString();

        usePresetsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPresetValues();
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createVolunteerActvity();
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    private void setPresetValues(){
        name.setText("Default activity name");
        description.setText("Default description");
        disclaimer.setText("Default disclaimer");
        min_age.setText("18");
        //type.setText("Environmental");
        desired_gender.setText("N/A");
        contact_phone.setText("12344321");
        contact_mail.setText("contact@contact.com");
        contact_name.setText("Default contact name");
        start_date.setText("1/1/2015");
        end_date.setText("31/12/2015");
        required_persons.setText("20");
        location.setText("Costa Rica");
    }

    private void createVolunteerActvity(){
        VolunteerActivity newActivity = new VolunteerActivity();
        newActivity.setId(9999);

        newActivity.setName(name.getText().toString());
        newActivity.setDescription(description.getText().toString());
        newActivity.setDisclaimer(disclaimer.getText().toString());

        newActivity.setMinAge(Integer.valueOf(min_age.getText().toString()));
        newActivity.setType(type.getSelectedItem().toString());
        newActivity.setDesiredGender(desired_gender.getText().toString());

        newActivity.setContactPhone(contact_phone.getText().toString());
        newActivity.setContactMail(contact_mail.getText().toString());
        newActivity.setContactName(contact_name.getText().toString());

        newActivity.setStartDate(start_date.getText().toString());
        newActivity.setEndDate(end_date.getText().toString());
        newActivity.setRequiredPersons(Integer.valueOf(required_persons.getText().toString()) );

        newActivity.setAvailableSpaces(Integer.valueOf(required_persons.getText().toString()) );
        newActivity.setLocation(location.getText().toString());
        //TODO: Replace for the real app user
        User user= makeDummyUser();
        newActivity.setOwner(makeDummyUser());

        Call<VolunteerActivity> call = BluemixClient.getApiService().createActivity(
                name.getText().toString(),
                description.getText().toString(),
                disclaimer.getText().toString(),
                Integer.valueOf(min_age.getText().toString()),
                type.getSelectedItem().toString(),
                desired_gender.getText().toString(),
                contact_phone.getText().toString(),
                contact_mail.getText().toString(),
                contact_name.getText().toString(),
                start_date.getText().toString(),
                end_date.getText().toString(),
                Integer.valueOf(required_persons.getText().toString()),
                Integer.valueOf(required_persons.getText().toString()),
                location.getText().toString(),
                user.getId());

        call.enqueue(new Callback<VolunteerActivity>() {
            @Override
            public void onResponse(Response<VolunteerActivity> response) {
                onActivityCreated();
            }

            @Override
            public void onFailure(Throwable t) {
                warnServiceError();
            }
        });
    }

    public User makeDummyUser(){
        User user = new User("Juan", "j@j.com", "passJ", "1234 5678", "Costa Rica", "Voluntario", "Masculino", 25);
        user.setId(401);
        return user;
    }

    private void onActivityCreated(){
        Toast.makeText(getActivity(), getString(R.string.info_activity_created), Toast.LENGTH_LONG).show();
    }

    private void warnServiceError(){
        Toast.makeText(getActivity(), getString(R.string.error_activities_not_acquired), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
