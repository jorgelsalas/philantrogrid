package com.hack.ibm.philantrogrid.entities;

/*
 * define O-R mapping of user request table
 */
public class RequiredSkillForActivity {

	long id;
    private Skill skill;
    private VolunteerActivity activity;


    public RequiredSkillForActivity(){
    	
    }
    
	public long getId() {
		return id;
	}

	public void setId(long pk) {
		id = pk;
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	public VolunteerActivity getActivity() {
		return activity;
	}

	public void setActivity(VolunteerActivity activity) {
		this.activity = activity;
	}

	@Override
    public String toString() {
        return String.format("{\"id\": \"%d\", " +
                "\"skill\": \"%s\", " +
                "\"activity\": \"%s\"" +
                "}", id, (null!=skill)?skill.toString():"null", (null!=activity)?activity.toString():"null");
    }
    
    public void setsAll(Skill skill, VolunteerActivity activity){
        setSkill(skill);
        setActivity(activity);
    }
}
