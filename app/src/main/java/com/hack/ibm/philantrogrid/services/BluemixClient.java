package com.hack.ibm.philantrogrid.services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;
import com.hack.ibm.philantrogrid.constants.ServiceConstants;
import com.hack.ibm.philantrogrid.interfaces.BluemixService;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by Jorge on 20/09/2015.
 */
public class BluemixClient {


    private static BluemixService apiService = null;

    public BluemixClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(ServiceConstants.BASE_URL)
                .build();
        apiService = retrofit.create(BluemixService.class);
    }


    public static BluemixService getApiService() {
        if(null==apiService){
            Gson gson = new GsonBuilder()
                    .registerTypeAdapterFactory((TypeAdapterFactory) new ItemTypeAdapterFactory()) // This is the important line ;)
                    .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .baseUrl(ServiceConstants.BASE_URL)
                    .build();
            apiService = retrofit.create(BluemixService.class);
        }
        return apiService;
    }
}
