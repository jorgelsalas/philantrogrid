package com.hack.ibm.philantrogrid;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.internal.InternalSettings;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.hack.ibm.philantrogrid.activities.MainActivity;
import com.hack.ibm.philantrogrid.asynctasks.BluemixAsyncTask;
import com.hack.ibm.philantrogrid.helpers.LoggedUserHelper;

import java.util.Arrays;


public class LoginActivity extends ActionBarActivity {

    /*
    * IMPORTANT TODOS:
    * */
    //TODO: Ver como agregar actividades al usuario
    //TODO: Ver como bluemix puede devolver el usuario con sus actvidades
    //TODO: Ver el update de usuario cuando se agrega en una actividad (lado bluemix)

    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private ShareButton shareButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        Log.e("KEYHASH FOR APP:", FacebookSdk.getApplicationSignature(getApplicationContext()));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //new BluemixAsyncTask(this).execute("");
        LoggedUserHelper.getLoggedUser();

        shareButton = (ShareButton) findViewById(R.id.share_button);

        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        shareButton.setShareContent(content);


        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareContent();
            }
        });

        callbackManager = CallbackManager.Factory.create();
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        // If using in a fragment
        //loginButton.setFragment(this);
        // Other app specific specialization

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                showToast("Successful Login");
                Log.e("FB LOGIN SUCCESS", loginResult.toString());

            }

            @Override
            public void onCancel() {
                // App code
                showToast("Login cancelled");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                showToast("Failed Login");
                Log.e("FB LOGIN ERROR", exception.toString());
            }
        });

        Intent startMain = new Intent(this, MainActivity.class);
        startActivity(startMain);
    }

    public void shareContent(){
        LoginManager.getInstance().logInWithPublishPermissions(
                this,
                Arrays.asList("publish_actions"));
        //shareButton.setShareContent(content);
    }

    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
