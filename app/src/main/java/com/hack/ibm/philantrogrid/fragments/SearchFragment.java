package com.hack.ibm.philantrogrid.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.hack.ibm.philantrogrid.ActivityAdapter;
import com.hack.ibm.philantrogrid.R;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.interfaces.OnFragmentInteractionListener;
import com.hack.ibm.philantrogrid.services.BluemixClient;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private ListView activityList;
    private Button searchButton;
    private EditText searchEditText;
    private ActivityAdapter adapter;
    private ProgressBar progressBar;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        searchButton = (Button) v.findViewById(R.id.search_button);
        searchEditText = (EditText) v.findViewById(R.id.search_edit_text);

        progressBar = (ProgressBar) v.findViewById(R.id.progressbar);
        progressBar.setIndeterminate(true);


        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchActivityFromText();
            }
        });

        activityList = (ListView) v.findViewById(R.id.activity_list);
        adapter = new ActivityAdapter(getActivity());
        activityList.setAdapter(adapter);
        fetchAllActivities();
        return v;
    }

    private void searchActivityFromText(){
        String description = searchEditText.getText().toString();
        if(description.equals("")){
            Toast.makeText(getActivity(), getString(R.string.error_search_field_empty), Toast.LENGTH_LONG).show();
        }
        else{
            adapter.updateData(new ArrayList<VolunteerActivity>());
            progressBar.setVisibility(View.VISIBLE);
            fetchActivitiesWithDescription(description);
        }
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void fetchActivitiesWithDescription(String description){
        Call<ArrayList<VolunteerActivity>> call = BluemixClient.getApiService().getVolunteerActivitiesFromDescription(description);
        call.enqueue(new Callback<ArrayList<VolunteerActivity>>() {
            @Override
            public void onResponse(Response<ArrayList<VolunteerActivity>> response) {
                loadActivities(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                warnServiceError();
            }
        });
    }

    private void fetchAllActivities(){
        Call<ArrayList<VolunteerActivity>> call = BluemixClient.getApiService().getVolunteerActivities();
        call.enqueue(new Callback<ArrayList<VolunteerActivity>>() {
            @Override
            public void onResponse(Response<ArrayList<VolunteerActivity>> response) {
                loadActivities(response.body());
            }

            @Override
            public void onFailure(Throwable t) {
                warnServiceError();
            }
        });
    }

    private void loadActivities(ArrayList<VolunteerActivity> newActivities){
        adapter.updateData(newActivities);
        progressBar.setVisibility(View.GONE);
    }

    private void warnServiceError(){
        Toast.makeText(getActivity(), getString(R.string.error_activities_not_acquired), Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
    }

}
