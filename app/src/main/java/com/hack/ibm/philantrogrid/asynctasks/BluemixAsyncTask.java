package com.hack.ibm.philantrogrid.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.hack.ibm.philantrogrid.entities.Skill;
import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.services.BluemixClient;

import java.io.IOException;
import java.util.ArrayList;

import retrofit.Call;
import retrofit.Response;

/**
 * Created by Jorge on 20/09/2015.
 */
public class BluemixAsyncTask extends AsyncTask<String, Integer, String> {

    private Context context;
    ArrayList<Skill> skills = null;

    public BluemixAsyncTask(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        Call<ArrayList<Skill>> call = BluemixClient.getApiService().getSkills();
        Call<Skill> call2 = BluemixClient.getApiService().getSkill(1);
        Call<ArrayList<User>> call3 = BluemixClient.getApiService().getUsers();
        Call<User> call4 = BluemixClient.getApiService().getUser(401);
        Call<ArrayList<VolunteerActivity>> call5 = BluemixClient.getApiService().getVolunteerActivities();
        Call<VolunteerActivity> call6 = BluemixClient.getApiService().getVolunteerActivity(451);

        //POSTS
        Call<Skill> call7 = BluemixClient.getApiService().createSkill(makeDummySkill());
        Call<User> call8 = BluemixClient.getApiService().createUser(makeDummyUser());
        Call<VolunteerActivity> call9 = BluemixClient.getApiService().createActivity(makeDummyActivity());
        Call<Skill> call10 = BluemixClient.getApiService().createSkill(999, "Skill from phone", "sent from phone");
        try {
            Response response =  call.execute();
            logResponseInfo(response);
            skills = (ArrayList<Skill>) response.body();

            response = call2.execute();
            Log.e("SKILL FROM CALL 2", ((Skill) response.body()).toString());

            response = call3.execute();
            Log.e("USERS FROM CALL 3", ((ArrayList<User>) response.body()).toString());

            response = call4.execute();
            Log.e("USER FROM CALL 4", ((User) response.body()).toString());
            response = call5.execute();
            Log.e("ACTIVITIES FROM CALL 5", ((ArrayList<VolunteerActivity>) response.body()).toString());
            response = call6.execute();
            Log.e("ACTIVITIES FROM CALL 6", ((VolunteerActivity) response.body()).toString());

            //POSTS
            response = call7.execute();
            logResponseInfo(response);
            response = call8.execute();
            logResponseInfo(response);
            response = call9.execute();
            logResponseInfo(response);
            response = call10.execute();
            logResponseInfo(response);
            Skill newSkill = (Skill) response.body();

            //TODO: Si devuelve el valor correcto con el id real y no el hardcoded al enviarse
            Log.e("SKILLLLLL RETURNED", newSkill.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    protected void onPostExecute(String result) {
        if (null == result) {

        } else {
            Log.e("SKILLS: ", skills.toString());
        }
    }

    public User makeDummyUser(){
        User user = new User("Dummy user", "Dummy mail", "dummy pass", "12365478", "dummy country", "dummy volunteer", "dummy male", 39);
        user.setId(999);
        return user;
    }

    public Skill makeDummySkill(){
        Skill skill = new Skill();
        skill.setId(999);
        skill.setDescription("dummy skill from phone");
        skill.setName("Dummy skill");
        return skill;
    }

    public VolunteerActivity makeDummyActivity(){
        VolunteerActivity activity = new VolunteerActivity("dummy VA", "dummy VA from phone", "dummy disclaimer", 18, "dummy environmental", "dummy male", "12344488",
                "dummy cMail", "dummy cName", "dummy startDate", "dummyEndDate", 20, 15, "dummy Location", makeDummyUser());
        activity.setId(9999);
        return activity;
    }

    public void logResponseInfo(Response response) throws IOException {
        Log.e("Message: ", response.message());
        if(null != response.errorBody()) Log.e("error body string: ", response.errorBody().string());
        if(null != response.errorBody()) Log.e("error body to string:", response.errorBody().toString());

        Log.e("response to string:", response.toString());
        //Log.e("body to string:", response.body().toString());
        Log.e("Code:", "" + response.code());
        Log.e("Headers to string:", response.headers().toString());
        Log.e("isSuccess to string:", "" + response.isSuccess());
        Log.e("raw:", "" + response.raw());
        Log.e("raw to string:", "" + response.raw().toString());
        Log.e("response to string:", response.toString());
    }
}
