package com.hack.ibm.philantrogrid.interfaces;

import com.hack.ibm.philantrogrid.entities.Skill;
import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;

import java.util.ArrayList;

/**
 * Created by Jorge on 20/09/2015. To react when the service responds.
 */
public interface BluemixListener {

    void receiveUsers(ArrayList<User> users);
    void receiveUser(User user);
    void receiveSkills(ArrayList<Skill> skills);
    void receiveSkill(Skill skill);
    void receiveVolunteerActivities(ArrayList<VolunteerActivity> volunteerActivities);
    void receiveVolunteerActivity(VolunteerActivity volunteerActivity);
}
