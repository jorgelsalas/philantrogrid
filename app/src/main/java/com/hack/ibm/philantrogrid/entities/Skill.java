package com.hack.ibm.philantrogrid.entities;

import java.util.ArrayList;

/*
 * define O-R mapping of todolist table
 */
public class Skill {

	long id;
	String name;
	String description;
	private ArrayList<RequiredSkillForActivity> activitiesRequiringThisSkill = new ArrayList<RequiredSkillForActivity>();
	
	public Skill(){
		
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long pk) {
		id = pk;
	}
	
	public ArrayList<RequiredSkillForActivity> getActivitiesRequiringThisSkill() {
		return activitiesRequiringThisSkill;
	}

	public void setActivitiesRequiringThisSkill(ArrayList<RequiredSkillForActivity> activitiesRequiringThisSkill) {
		this.activitiesRequiringThisSkill = activitiesRequiringThisSkill;
	}
	

	@Override
	public String toString() {
		return String.format("{\"id\": \"%d\", \"name\": \"%s\", \"description\": \"%s\"}", id, name, description);
	}
}
