package com.hack.ibm.philantrogrid.constants;

/**
 * Created by Jorge on 30/09/2015. General constants
 */
public class GeneralConstants {

    public final static String ACTIVITY_TYPE_ENVIRONMENTAL = "Environmental";
    public final static String ACTIVITY_TYPE_EDUCATIONAL = "Educational";
    public final static String ACTIVITY_TYPE_GOOD_WILL = "Goodwill";
    public final static String ACTIVITY_TYPE_HEALTH = "Health";
}
