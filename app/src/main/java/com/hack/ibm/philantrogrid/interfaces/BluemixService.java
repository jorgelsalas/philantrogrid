package com.hack.ibm.philantrogrid.interfaces;

import com.hack.ibm.philantrogrid.constants.ServiceConstants;
import com.hack.ibm.philantrogrid.entities.Skill;
import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

/**
 * Created by Jorge on 20/09/2015.
 */
public interface BluemixService {

    //@GET(ServiceConstants.USERS_ENDPOINT)
    //Call<User> getUser(@Query("id"))

    @GET(ServiceConstants.USERS_ENDPOINT)
    Call<ArrayList<User>> getUsers();

    @GET(ServiceConstants.USERS_ENDPOINT)
    Call<User> getUser(@Query(ServiceConstants.ID_PARAM) long id);

    @POST(ServiceConstants.USERS_ENDPOINT)
    Call<User> createUser(@Body User newUser);


    @GET(ServiceConstants.ACTIVITIES_ENDPOINT)
    Call<ArrayList<VolunteerActivity>> getVolunteerActivities();

    @GET(ServiceConstants.SEARCH_ENDPOINT)
    Call<ArrayList<VolunteerActivity>> getVolunteerActivitiesFromDescription(@Query(ServiceConstants.DESCRIPTION_PARAM) String description);

    //TODO: AVERIGUAR COMO IMPLEMENTAR METODO EN BLUEMIX
    @FormUrlEncoded
    @PUT(ServiceConstants.SUBSCRIBE_ENDPOINT)
    Call<VolunteerActivity> subscribeToActivity(@Field(ServiceConstants.SUBSCRIBE_USER_ID_PARAM) long id,
                                                @Field(ServiceConstants.SUBSCRIBE_ACTIVITY_ID_PARAM) long act_id);

    @GET(ServiceConstants.SUBSCRIBE_ENDPOINT)
    Call<ArrayList<VolunteerActivity>> getVolunteerActivitiesFromUserId(@Query(ServiceConstants.ID_PARAM) long id);


    @GET(ServiceConstants.ACTIVITIES_ENDPOINT)
    Call<VolunteerActivity> getVolunteerActivity(@Query(ServiceConstants.ID_PARAM) long id);

    @POST(ServiceConstants.ACTIVITIES_ENDPOINT)
    Call<VolunteerActivity> createActivity(@Body VolunteerActivity newActivity);

    @FormUrlEncoded
    @POST(ServiceConstants.ACTIVITIES_ENDPOINT)
    Call<VolunteerActivity> createActivity(
            @Field("name") String name, @Field("description") String description, @Field("disclaimer") String disclaimer, @Field("minAge") int minAge,
            @Field("type") String type, @Field("desiredGender") String desiredGender, @Field("contactPhone") String contactPhone, @Field("contactMail") String contactMail,
            @Field("contactName") String contactName, @Field("startDate") String startDate, @Field("endDate") String endDate, @Field("requiredPersons") int requiredPersons,
            @Field("availableSpaces") int availableSpaces, @Field("location") String location, @Field("owner") long owner

    );

    @GET(ServiceConstants.SKILLS_ENDPOINT)
    Call<ArrayList<Skill>> getSkills();

    @GET(ServiceConstants.SKILLS_ENDPOINT)
    Call<Skill> getSkill(@Query(ServiceConstants.ID_PARAM) long id);

    //TODO: Los que llevan body no estan funcionando... hay que hacerlo "a pie" con los @Fields
    @Headers({
            "Content-type: application/json"
    })
    @POST(ServiceConstants.SKILLS_ENDPOINT)
    Call<Skill> createSkill(@Body Skill newSkill);

    @FormUrlEncoded
    @POST(ServiceConstants.SKILLS_ENDPOINT)
    Call<Skill> createSkill(@Field("id") long id, @Field("name") String name, @Field("description") String description);
}
