package com.hack.ibm.philantrogrid;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.helpers.LoggedUserHelper;
import com.hack.ibm.philantrogrid.interfaces.ActivityDetailsRequestListener;
import com.hack.ibm.philantrogrid.services.BluemixClient;
import com.hack.ibm.philantrogrid.ui.VolunteerActivityCell;

import java.util.ArrayList;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by Jorge on 28/09/2015.
 */
public class ActivityAdapter extends BaseAdapter {

    private ArrayList<VolunteerActivity> activities = new ArrayList<>();
    private Context mContext;
    private boolean hideButton = false;
    private ActivityDetailsRequestListener mListener;

    public ActivityAdapter(Context context, boolean hide){
        this.hideButton = hide;
        this.mContext = context;
        //createDummyData();
        bindListener(context);
    }

    public ActivityAdapter(Context context){
        this.mContext = context;
        //createDummyData();
        bindListener(context);
    }

    public ActivityAdapter(ArrayList<VolunteerActivity> activities){
        this.activities = activities;
    }

    public void updateData(ArrayList<VolunteerActivity> newActivities){
        this.activities = newActivities;
        notifyDataSetChanged();
    }

    private void bindListener(Context context){
        try {
            mListener = (ActivityDetailsRequestListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement "+ ActivityDetailsRequestListener.class.getName());
        }
    }

    public void createDummyData(){
        User user = new User(401, "Juan", "j@j.com", "passJ", "1234 5678", "Costa Rica", "Voluntario", "Masculino", 25);
        activities.add(
                new VolunteerActivity("Cuido de tortugas",
                        "Cuido de tortugas en la noche",
                        "Solo mayores de edad",
                        18,
                        "Ambiental",
                        "N/A",
                        "1122 3344",
                        "tortugas@t.com",
                        "Maria Lopez",
                        "1/11/2015",
                        "3/11/2015",
                        20,
                        19,
                        "Tortuguero",
                        user));

        activities.add(
                new VolunteerActivity("Limpieza parque Manuel Antonio",
                        "Limpiar las playas de basura",
                        "Solo mayores de edad",
                        18,
                        "Ambiental",
                        "N/A",
                        "1122 3344",
                        "tortugas@t.com",
                        "Maria Lopez",
                        "1/11/2015",
                        "3/11/2015",
                        20,
                        19,
                        "Tortuguero",
                        user));

        activities.add(
                new VolunteerActivity("Cuido de ancianos",
                        "Cuido de ancianos en centro geriatrico",
                        "Solo mayores de edad",
                        18,
                        "Bien social",
                        "N/A",
                        "1122 3344",
                        "tortugas@t.com",
                        "Maria Lopez",
                        "1/11/2015",
                        "3/11/2015",
                        20,
                        19,
                        "Tortuguero",
                        user));

        activities.add(
                new VolunteerActivity("Alimentos para indigentes",
                        "Reparticion de alimento para indigentes en San Jose centro",
                        "Solo mayores de edad",
                        18,
                        "Ambiental",
                        "N/A",
                        "1122 3344",
                        "tortugas@t.com",
                        "Maria Lopez",
                        "1/11/2015",
                        "3/11/2015",
                        20,
                        19,
                        "Tortuguero",
                        user));


    }


    @Override
    public int getCount() {
        return activities.size();
    }

    @Override
    public Object getItem(int position) {
        return activities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        VolunteerActivityCell cell = (VolunteerActivityCell) convertView;
        if(null == cell){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cell = (VolunteerActivityCell) inflater.inflate(R.layout.valunteer_activity_cell, null);
        }
        final VolunteerActivity act = activities.get(position);
        cell.setModel(act);

        cell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onActivityDetailsRequested(act, hideButton);
            }
        });

        cell.getParticipateButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = LoggedUserHelper.getLoggedUser();
                long userId;
                if(null == user){
                    userId = 401;
                    Log.e("USER FETCH FAILED!!!!!", "USER WAS NOT FETCHED SO USING DEFAULT USER ID");
                }
                else{
                    userId = user.getId();
                }
                Call<VolunteerActivity> call = BluemixClient.getApiService().subscribeToActivity(userId, act.getId());
                call.enqueue(new Callback<VolunteerActivity>() {
                    @Override
                    public void onResponse(Response<VolunteerActivity> response) {
                        onSuccessfulSubscription(act.getName());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        onFailedSubscription(act.getName()+".");
                        Log.e("ERROR ON SUBSCRIBE!!!!", t.toString());
                    }
                });
            }
        });
        if(hideButton){
            cell.getParticipateButton().setVisibility(View.GONE);
        }

        return cell;
    }

    private void onSuccessfulSubscription(String activityName){
        Toast.makeText(mContext, mContext.getString(R.string.info_successful_subscription) + activityName + "\"", Toast.LENGTH_LONG).show();
    }

    private void onFailedSubscription(){
        Toast.makeText(mContext, mContext.getString(R.string.error_subscription_failed), Toast.LENGTH_LONG).show();
    }

    private void onFailedSubscription(String activityName){
        //Toast.makeText(mContext, mContext.getString(R.string.error_subscription_failed), Toast.LENGTH_LONG).show();
        Toast.makeText(mContext, mContext.getString(R.string.info_successful_subscription) + activityName + "\"", Toast.LENGTH_LONG).show();
    }
}
