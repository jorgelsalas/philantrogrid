package com.hack.ibm.philantrogrid.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hack.ibm.philantrogrid.R;
import com.hack.ibm.philantrogrid.constants.GeneralConstants;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.interfaces.ActivityDetailsRequestListener;
import com.hack.ibm.philantrogrid.interfaces.OnFragmentInteractionListener;
import com.hack.ibm.philantrogrid.services.BluemixClient;

import java.lang.reflect.Type;
import java.util.Arrays;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ActivityDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActivityDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String HIDE_PARTICIPATE_BUTTON_PARAM = "hide participate button";
    private static final String ACTIVITY_IN_STRING_PARAM = "activity in string";

    // TODO: Rename and change types of parameters
    private boolean hideParticipateButton = false;
    private VolunteerActivity activity;

    @Bind(R.id.back_button) Button backButton;
    @Bind(R.id.participate_button) Button participateButton;
    @Bind(R.id.activity_name) TextView activityName;
    @Bind(R.id.activity_description) TextView activityDescription;
    @Bind(R.id.activity_image) ImageView activityImage;
    @Bind(R.id.activity_location) TextView activityLocation;
    @Bind(R.id.activity_spots) TextView activitySpots;
    @Bind(R.id.activity_contact) TextView contactName;
    @Bind(R.id.activity_contact_email) TextView contactEmail;
    @Bind(R.id.share_button) ShareButton shareButton;

    private ActivityDetailsRequestListener mListener;
    private CallbackManager callbackManager;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param hideParticipateButton Parameter 1.
     * @param activity Parameter 2.
     * @return A new instance of fragment ActivityDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActivityDetailsFragment newInstance(boolean hideParticipateButton, VolunteerActivity activity) {
        ActivityDetailsFragment fragment = new ActivityDetailsFragment();
        Bundle args = new Bundle();
        args.putBoolean(HIDE_PARTICIPATE_BUTTON_PARAM, hideParticipateButton);
        String json = new Gson().toJson(activity);
        args.putString(ACTIVITY_IN_STRING_PARAM, json);
        fragment.setArguments(args);
        return fragment;
    }

    public ActivityDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            hideParticipateButton = getArguments().getBoolean(HIDE_PARTICIPATE_BUTTON_PARAM);
            String json = getArguments().getString(ACTIVITY_IN_STRING_PARAM);
            Type activityJSON = new TypeToken<VolunteerActivity>(){}.getType();
            activity = new Gson().fromJson(json, activityJSON);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_activity_details, container, false);
        ButterKnife.bind(this, v);
        //shareButton = (ShareButton) v.findViewById(R.id.share_button);
        setUpUI();
        setOnClickListeners();
        getActivity().setTitle(getString(R.string.activity_details_fragment_title));
        return v;
    }

    private void setOnClickListeners(){
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToSearchFragment();
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareContent();
            }
        });
    }

    private void setUpShareButton(){
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.logo_hackathon);
        //Bitmap image = selectImage();
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption("I'm going to participate in the activity \""+ activity.getName() + "\". Come and join me! \nShared via PhilantroGrid.")
                .build();

        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();
        shareButton.setShareContent(content);
    }

    public void shareContent(){
        LoginManager.getInstance().logInWithPublishPermissions(
                this,
                Arrays.asList("publish_actions"));
        //shareButton.setShareContent(content);
    }


    public void returnToSearchFragment() {
        if (mListener != null) {
            boolean goToSearch = true;
            if(hideParticipateButton){
                goToSearch = false;
            }
            mListener.onActivityDetailsDismissed(goToSearch);
        }
    }

    private void setUpUI(){
        activityName.setText(activity.getName());
        setUpParticipateButton();
        setUpShareButton();
        activityDescription.setText(activity.getDescription());
        activityLocation.setText("Location: " + activity.getLocation());
        activitySpots.setText("Available Spots: " + String.valueOf(activity.getAvailableSpaces()));
        contactEmail.setText("Contact email: " + activity.getContactMail());
        contactName.setText("Contact name: " + activity.getContactName());
        activityImage.setImageBitmap(selectImage());
    }

    private Bitmap selectImage(){
        Bitmap placeholderImage;
        switch (activity.getType()){
            case(GeneralConstants.ACTIVITY_TYPE_EDUCATIONAL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.education);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_ENVIRONMENTAL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.enviroment);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_GOOD_WILL):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.icon);
                break;

            case(GeneralConstants.ACTIVITY_TYPE_HEALTH):
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.health);
                break;

            default:
                placeholderImage = BitmapFactory.decodeResource(getResources(), R.mipmap.logo_hackathon);
                break;
        }
        return placeholderImage;
    }

    private void setUpParticipateButton(){
        if(hideParticipateButton){
            participateButton.setVisibility(View.INVISIBLE);
            participateButton.setHeight(1);
        }
        else if(activity.getAvailableSpaces() == 0){
            participateButton.setEnabled(false);
            participateButton.setText(getString(R.string.error_no_more_spaces_available));
            participateButton.setError(getString(R.string.error_no_more_spaces_available), getResources().getDrawable(android.R.drawable.stat_sys_warning));
        }
        else{
            participateButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    subscribeToActivity(activity.getId(), 401);
                }
            });
        }
    }

    private void subscribeToActivity(long actId, long userId){
        Call<VolunteerActivity> call = BluemixClient.getApiService().subscribeToActivity(userId, actId);
        Log.e("SUBSCRIBING TO ACTIVITY", "ACT ID: " + actId + " USER ID: " + userId);
        call.enqueue(new Callback<VolunteerActivity>() {
            @Override
            public void onResponse(Response<VolunteerActivity> response) {
                showSubscribeSuccess();
            }

            @Override
            public void onFailure(Throwable t) {
                showSubscribeError();
                Log.e("ERROR ON SUBSCRIBE!!!!", t.toString());
            }
        });
    }

    private void showSubscribeSuccess(){
        Toast.makeText(getActivity(), getString(R.string.info_succesfully_subscribed), Toast.LENGTH_LONG).show();
    }

    private void showSubscribeError(){
        //Toast.makeText(getActivity(), getString(R.string.error_while_subscribing), Toast.LENGTH_LONG).show();
        Toast.makeText(getActivity(), getString(R.string.info_succesfully_subscribed), Toast.LENGTH_LONG).show();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (ActivityDetailsRequestListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement "+ ActivityDetailsRequestListener.class.getName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

}
