package com.hack.ibm.philantrogrid.helpers;

import com.hack.ibm.philantrogrid.entities.User;
import com.hack.ibm.philantrogrid.services.BluemixClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by Jorge on 01/10/2015.
 */
public class LoggedUserHelper {

    private static User user = null;

    public static User getLoggedUser(){
        if(null == user){
            downloadUserData(401);
            return null;
        }
        else{
            return user;
        }
    }

    public static void updateUser(User newUser){
        user = newUser;
    }

    public static void downloadUserData(long id){
        Call<User> call = BluemixClient.getApiService().getUser(id);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Response<User> response) {
                user = response.body();
            }

            @Override
            public void onFailure(Throwable t) {
                user = null;
            }
        });
    }


}
