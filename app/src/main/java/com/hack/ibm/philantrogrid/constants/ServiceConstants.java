package com.hack.ibm.philantrogrid.constants;

/**
 * Created by Jorge on 20/09/2015.
 */
public class ServiceConstants {

    public static final String BASE_URL = "http://cherrywithdb.mybluemix.net/api/";
    public static final String USERS_ENDPOINT = "users";
    public static final String ACTIVITIES_ENDPOINT = "activities";
    public static final String SKILLS_ENDPOINT = "skills";
    public static final String SUBSCRIBE_ENDPOINT = "subscribe";
    public static final String SEARCH_ENDPOINT = "search";

    public static final String ID_PARAM = "id";
    public static final String NAME_PARAM = "name";
    public static final String DESCRIPTION_PARAM = "description";
    public static final String MIN_AGE_PARAM = "minAge";
    public static final String TYPE_PARAM = "type";
    public static final String START_DATE_PARAM = "startDate";
    public static final String LOCATION_PARAM = "location";

    public static final String SUBSCRIBE_ACTIVITY_ID_PARAM = "act_id";
    public static final String SUBSCRIBE_USER_ID_PARAM = "id";



}
