package com.hack.ibm.philantrogrid.activities;

import android.app.Activity;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hack.ibm.philantrogrid.R;
import com.hack.ibm.philantrogrid.entities.VolunteerActivity;
import com.hack.ibm.philantrogrid.fragments.ActivityDetailsFragment;
import com.hack.ibm.philantrogrid.fragments.CreateFragment;
import com.hack.ibm.philantrogrid.fragments.MyActivitiesFragment;
import com.hack.ibm.philantrogrid.fragments.SearchFragment;
import com.hack.ibm.philantrogrid.fragments.SettingsFragment;
import com.hack.ibm.philantrogrid.interfaces.ActivityDetailsRequestListener;
import com.hack.ibm.philantrogrid.interfaces.OnFragmentInteractionListener;

public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnFragmentInteractionListener, ActivityDetailsRequestListener {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getString(R.string.search_fragment_title);
        setTitle(getString(R.string.search_fragment_title));

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentID = "";
        Fragment fragment;

        switch (position) {
            case 0:
                fragment = new SearchFragment();
                fragmentID = getString(R.string.search_fragment_id);
                break;
            case 1:
                fragment = new MyActivitiesFragment();
                fragmentID = getString(R.string.my_activities_fragment_id);
                break;
            case 2:
                fragment = new CreateFragment();
                fragmentID = getString(R.string.create_fragment_id);
                break;

            case 3:
                fragment = new SettingsFragment();
                fragmentID = getString(R.string.settings_fragment_id);
                break;

            default:
                fragment = new SettingsFragment();
                fragmentID = getString(R.string.settings_fragment_id);
                break;
        }
        fragmentManager.beginTransaction().replace(R.id.container, fragment, fragmentID).commit();
        onSectionAttached(position);

    }

    public void onSectionAttached(int number) {
        switch (number) {
            case 0:
                mTitle = getString(R.string.search_fragment_title);
                break;
            case 1:
                mTitle = getString(R.string.my_activities_fragment_title);
                break;
            case 2:
                mTitle = getString(R.string.create_fragment_title);
                break;
            case 3:
                mTitle = getString(R.string.settings_fragment_title);
                break;
            default:
                mTitle = getString(R.string.search_fragment_title);
                break;
        }
        setTitle(mTitle);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentInteraction(String id) {

    }

    @Override
    public void onActivityDetailsRequested(VolunteerActivity activity, boolean hideParticipateButton) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String fragmentID = getString(R.string.activity_details_fragment_id);
        Fragment fragment = ActivityDetailsFragment.newInstance(hideParticipateButton, activity);
        fragmentManager.beginTransaction().replace(R.id.container, fragment, fragmentID).commit();

    }

    @Override
    public void onActivityDetailsDismissed() {
        onNavigationDrawerItemSelected(0);
    }

    @Override
    public void onActivityDetailsDismissed(boolean goToSearch) {
        if(goToSearch){
            onNavigationDrawerItemSelected(0);
        }
        else{
            onNavigationDrawerItemSelected(1);
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(
                    getArguments().getInt(ARG_SECTION_NUMBER));
        }
    }

}
