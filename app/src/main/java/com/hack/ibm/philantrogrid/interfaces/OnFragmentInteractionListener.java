package com.hack.ibm.philantrogrid.interfaces;

import android.net.Uri;

/**
 * Created by Jorge on 28/09/2015.
 */
public interface OnFragmentInteractionListener {
    public void onFragmentInteraction(Uri uri);
    public void onFragmentInteraction(String id);
}
