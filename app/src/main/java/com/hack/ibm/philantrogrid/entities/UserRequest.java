package com.hack.ibm.philantrogrid.entities;


/*
 * define O-R mapping of user request table
 */
public class UserRequest {
	
	public static final String STATUS_PENDING = "Pending approval";
	public static final String STATUS_REJECTED = "Rejected";
	public static final String STATUS_APPROVED = "Approved";
	

	long id;
	String status;
	String comment;
    private User requester;
    private VolunteerActivity activity;
    
    public UserRequest() {
		super();
		
	}
	
	public UserRequest(long id, String status) {
		super();
		this.id = id;
		this.status = status;
	}

	public UserRequest(long id, String status, String comment) {
		super();
		this.id = id;
		this.status = status;
		this.comment = comment;
	}

	public UserRequest(long id, String status, String comment, User requester,
			VolunteerActivity activity) {
		super();
		this.id = id;
		this.status = status;
		this.comment = comment;
		this.requester = requester;
		this.activity = activity;
	}

	
	
    
	
	public long getId() {
		return id;
	}

	public void setId(long pk) {
		id = pk;
	}
	
	
    
    public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getRequester() {
		return requester;
	}

	public void setRequester(User requester) {
		this.requester = requester;
	}

	public VolunteerActivity getActivity() {
		return activity;
	}

	public void setActivity(VolunteerActivity activity) {
		this.activity = activity;
	}

	@Override
    public String toString() {
        return String.format("{\"id\": \"%d\", " +
                "\"status\": \"%s\", " +
                "\"comment\": \"%s\", " +
                "\"requester\": \"%s\", " +
                "\"activity\": \"%s\"" +
                "}", id, status, comment, (null!=requester)?requester.toString():"null", (null!=activity)?activity.toString():"null");
    }
    
    public void setsAll(String status, String comment, User requester, VolunteerActivity activity){
        setStatus(status);
        setComment(comment);
        setRequester(requester);
        setActivity(activity);
    }
}
