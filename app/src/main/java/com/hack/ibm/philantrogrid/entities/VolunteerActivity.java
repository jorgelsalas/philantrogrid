package com.hack.ibm.philantrogrid.entities;

import java.util.ArrayList;


/*
 * define O-R mapping of todolist table
 */
public class VolunteerActivity {

	long id;
	String name;
	String description;
	String disclaimer;
    int minAge;
    String type;
    String desiredGender;
    String contactPhone;
    String contactMail;
    String contactName;
    String startDate;
    String endDate;
    int requiredPersons;
    int availableSpaces;
    String location;

	private User owner = null;
	private ArrayList<UserRequest> comments = new ArrayList<UserRequest>();
	private ArrayList<RequiredSkillForActivity> requiredSkills = new ArrayList<RequiredSkillForActivity>();
    
    public VolunteerActivity(){
    	
    }
    
    public VolunteerActivity(String name, String description, String disclaimer, int minAge, String type, String desiredGender, String contactPhone, String contactMail, String contactName, String startDate, String endDate, int requiredPersons, int availableSpaces, String location) {
        this.name = name;
        this.description = description;
        this.disclaimer = disclaimer;
        this.minAge = minAge;
        this.type = type;
        this.desiredGender = desiredGender;
        this.contactPhone = contactPhone;
        this.contactMail = contactMail;
        this.contactName = contactName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.requiredPersons = requiredPersons;
        this.availableSpaces = availableSpaces;
        this.location = location;
        this.comments = new ArrayList<UserRequest>();
        this.requiredSkills = new ArrayList<RequiredSkillForActivity>();
    }
    
    public VolunteerActivity(String name, String description, String disclaimer, int minAge, String type, String desiredGender, String contactPhone, String contactMail, String contactName, String startDate, String endDate, int requiredPersons, int availableSpaces, String location, User owner) {
        this.name = name;
        this.description = description;
        this.disclaimer = disclaimer;
        this.minAge = minAge;
        this.type = type;
        this.desiredGender = desiredGender;
        this.contactPhone = contactPhone;
        this.contactMail = contactMail;
        this.contactName = contactName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.requiredPersons = requiredPersons;
        this.availableSpaces = availableSpaces;
        this.location = location;
        this.owner = owner;
        this.comments = new ArrayList<UserRequest>();
        this.requiredSkills = new ArrayList<RequiredSkillForActivity>();
    }
    
    
    
    
    public long getId() {
		return id;
	}

	public void setId(long pk) {
		id = pk;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	public String getDisclaimer() {
        return disclaimer;
    }

    public void setDisclaimer(String disclaimer) {
        this.disclaimer = disclaimer;
    }

    public int getMinAge() {
        return minAge;
    }

    public void setMinAge(int minAge) {
        this.minAge = minAge;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesiredGender() {
        return desiredGender;
    }

    public void setDesiredGender(String desiredGender) {
        this.desiredGender = desiredGender;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactMail() {
        return contactMail;
    }

    public void setContactMail(String contactMail) {
        this.contactMail = contactMail;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getRequiredPersons() {
        return requiredPersons;
    }

    public void setRequiredPersons(int requiredPersons) {
        this.requiredPersons = requiredPersons;
    }

    public int getAvailableSpaces() {
        return availableSpaces;
    }

    public void setAvailableSpaces(int availableSpaces) {
        this.availableSpaces = availableSpaces;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
    
    public ArrayList<UserRequest> getComments() {
		return comments;
	}

	public void setComments(ArrayList<UserRequest> comments) {
		this.comments = comments;
	}
	
    public ArrayList<RequiredSkillForActivity> getRequiredSkills() {
		return requiredSkills;
	}

	public void setRequiredSkills(ArrayList<RequiredSkillForActivity> requiredSkills) {
		this.requiredSkills = requiredSkills;
	}
	
	@Override
    public String toString() {
        return String.format("{\"id\": \"%d\", " +
                "\"name\": \"%s\", " +
                "\"description\": \"%s\", " +
                "\"disclaimer\": \"%s\", " +
                "\"minAge\": \"%d\", " +
                "\"type\": \"%s\", " +
                "\"desiredGender\": \"%s\", " +
                "\"contactPhone\": \"%s\", " +
                "\"contactMail\": \"%s\", " +
                "\"contactName\": \"%s\", " +
                "\"startDate\": \"%s\", " +
                "\"endDate\": \"%s\", " +
                "\"requiredPersons\": \"%d\", " +
                "\"availableSpaces\": \"%d\", " +
                "\"location\": \"%s\", " +
                "\"owner\": %s" +
                "}", id, name, description, disclaimer, minAge, type, desiredGender, contactPhone, contactMail,
                contactName, startDate, endDate, requiredPersons, availableSpaces, location, null != owner ? owner.toString() : "null");
    }
    
    public void setsAll(String name, String description, String disclaimer, int minAge, String type, String desiredGender, String contactPhone, String contactMail, String contactName, String startDate, String endDate, int requiredPersons, int availableSpaces, String location) {
        setName(name);
        setDescription(description);
        setDisclaimer(disclaimer);
        setMinAge(minAge);
        setType(type);
        setDesiredGender(desiredGender);
        setContactPhone(contactPhone);
        setContactMail(contactMail);
        setContactName(contactName);
        setStartDate(startDate);setEndDate(endDate);
        setRequiredPersons(requiredPersons);
        setAvailableSpaces(availableSpaces);
        setLocation(location);
    }
    
    public void setsAll(String name, String description, String disclaimer, int minAge, String type, String desiredGender, String contactPhone, String contactMail, String contactName, String startDate, String endDate, int requiredPersons, int availableSpaces, String location, User owner) {
        setName(name);
        setDescription(description);
        setDisclaimer(disclaimer);
        setMinAge(minAge);
        setType(type);
        setDesiredGender(desiredGender);
        setContactPhone(contactPhone);
        setContactMail(contactMail);
        setContactName(contactName);
        setStartDate(startDate);setEndDate(endDate);
        setRequiredPersons(requiredPersons);
        setAvailableSpaces(availableSpaces);
        setLocation(location);
        setOwner(owner);
    }
	
	//@Override
	//public String toString() {
	//	return String.format("{\"id\": \"%d\", \"name\": \"%s\", \"description\": \"%s\"}", id, name, description);
	//}
}
