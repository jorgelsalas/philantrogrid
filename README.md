
This project was created during IBM Costa Rica's first Bluemix Hackathon. 

This app seeks to promote community members helping each other with their unique skills. There are 2 kinds of users: Those who request help and those who contribute their skills to their community. By looking at a list of help requests, the latter con offer their help by contacting the former. User's can share events to their Facebook profiles in order to raise awareness and attract more people interested in helping. Some examples of help requests include math mentoring, babysitting, public space cleaning, etc. 
